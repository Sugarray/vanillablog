<?php

include_once('services/SessionChecker.php');
include_once('services/Config.php');
include_once('services/AvatarLoader.php');

if( $_SERVER['REQUEST_METHOD'] == 'GET' )
{
    $users_sql = "SELECT * from user";
    $articles_sql = "SELECT * from Article";

    $users_array=[];
    $articles_array = [];

    $users_result = mysqli_query($db, $users_sql);
    $articles_result = mysqli_query($db, $articles_sql);

    while($row = mysqli_fetch_array($users_result, MYSQLI_ASSOC))
    {
        $users_array[] = array("id" => $row['id'], "username"=>$row['username'], "password"=>$row['password'],
        "image" => $row['image_url'], "role" => $row['is_admin']);
    }

    while($row = mysqli_fetch_array($articles_result, MYSQLI_ASSOC))
    {
        $articles_array[] = array("id" => $row['id'], "title"=>$row['title'], "preface"=>$row['preface']);
    }

}

?>

<!doctype html>
<html lang="en">
<head>
    <?php include_once('views/HeadView.html'); ?>
</head>
<body>
    <?php require("views/NavigationView.php"); ?>
    <div class="container-fluid">
        <ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
            <li class="active"><a href="#users" data-toggle="tab">Users</a></li>
            <li><a href="#articles" data-toggle="tab">Articles</a></li>
        </ul>
        <div id="my-tab-content" class="tab-content">
            <div class="tab-pane active" id="users">
                <h1>Users</h1>

                <table class="table">
                    <thead>
                    <tr>
                        <th>Username</th>
                        <th>Password</th>
                        <th>Image</th>
                        <th>Administrator</th>
                        <th>Delete</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php foreach($users_array as $user): ?>
                            <tr>
                                <td><?php echo($user['username']) ?></td>
                                <td><?php echo($user['password']) ?></td>
                                <td>
                                    <img src="<?php echo($user['image']) ?>" alt="img" width="50px" height="50px">
                                </td>
                                    <?php if($user['role']): ?>
                                        <td><input type="checkbox" checked  readonly></td>
                                     <?php else : ?>
                                        <td><input type="checkbox" readonly></td>
                                    <?php endif; ?>
                                <td>
                                    <?php if($user['username'] == $_SESSION['login_user']): ?>
                                        <a  class="btn btn-primary disabled" href=<?php echo("services/DeleteUser.php?id=".$user['id'])?>>Delete</a>
                                    <?php else : ?>
                                        <a  class="btn btn-primary" href=<?php echo("services/DeleteUser.php?id=".$user['id'])?>>Delete</a>
                                    <?php endif; ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>

            </div>
            <div class="tab-pane" id="articles">
                <h1>Articles</h1>
                <table class="table">
                    <thead>
                <tr>
                    <th>Title</th>
                    <th>Preface</th>
                    <th>Link</th>
                    <th>Delete</th>
                </tr>
                <?php foreach($articles_array as $article): ?>
                    <tr>
                        <td><?php echo($article['title']) ?></td>
                        <td><?php echo($article['preface']) ?></td>
                        <td>
                            <a class="btn btn-primary" href=<?php echo("../Article.php?post=".$article['id'])?>>Article</a>
                        </td>
                        <td>
                            <a  class="btn btn-primary" href=<?php echo("services/DeleteArticle.php?id=".$article['id'])?>>Delete</a>
                        </td>
                    </tr>
                <?php endforeach; ?>

                </table>

            </div>
        </div>
    </div>


    <?php require("views/JsView.html") ?>
</body>
</html>

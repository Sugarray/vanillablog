<?php
ob_start();
session_start();
include_once("services/Config.php");
include_once("services/AvatarLoader.php");

    $error = '';
    if($_SERVER['REQUEST_METHOD'] == 'POST')
    {
        if (isset($_POST['login']) && !empty($_POST['username']) && !empty($_POST['password']))
        {

            $username = mysqli_real_escape_string($db,$_POST['username']);
            $password = mysqli_real_escape_string($db,$_POST['password']);

            $sql = "SELECT * FROM user WHERE username = '$username' and password = '$password'";
            $result = mysqli_query($db,$sql);
            $row = mysqli_fetch_array($result,MYSQLI_ASSOC);


            $count = mysqli_num_rows($result);

            // If result matched $myusername and $mypassword, table row must be 1 row

            if($count == 1) {
                $_SESSION['login_user'] = $username;
                $_SESSION['is_admin'] = $row['is_admin'];
                LoadAvatar();
                header("location: index.php");
            }
            else {
                $error = "Your Login Name or Password is invalid";
            }
        }
    }

?>

<!doctype html>
<html lang="en">
<?php require('views/HeadView.html'); ?>
<body>
<h2>Enter Username and Password</h2>



<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Login</div>
                <div class="panel-body">
                    <?php if(!empty($error)): ?>
                        <div class="alert alert-danger" role="alert">
                            <h4 class="help-block"><?php echo $error;?></h4>
                        </div>
                    <?php endif; ?>
                    <form class="form-horizontal" role="form" method="POST" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']);?>">

                        <div class="form-group">
                            <label class="col-md-4 control-label">Username</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="username" required autofocus>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label">Password</label>
                            <div class="col-md-6">
                                <input type="password" class="form-control" name="password">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary" name="login" >
                                    <i class="fa fa-btn fa-sign-in"></i>Login
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<?php require("views/JsView.html") ?>
</body>
</html>

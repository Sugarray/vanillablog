<?php
session_start();
include_once('services/SessionChecker.php');
include_once('services/Config.php');


$error = "";
$login = $_SESSION['login_user'];
$profile_row  = array();

if( $_SERVER['REQUEST_METHOD'] == 'GET' )
{
    $sql = "SELECT * FROM user WHERE username = '$login'";

    $profile_result = mysqli_query($db,$sql);
    $profile_row = mysqli_fetch_array($profile_result,MYSQLI_ASSOC);
}
else
{
    $update_sql = "UPDATE user SET";
    $sql = "SELECT * FROM user WHERE username = '$login'";
    $profile_result = mysqli_query($db,$sql);
    $profile_row = mysqli_fetch_array($profile_result,MYSQLI_ASSOC);
    $update_sequence = array();
    $login_before = $_SESSION['login_user'];

    if($_FILES['avatar']['size'] != 0)
    {

        $file_tmp = $_FILES['avatar']['tmp_name'];
        $file_ext=strtolower(end(explode('.',$_FILES['avatar']['name'])));
        $file_name = basename($_FILES['avatar']['name'], ".".$file_ext);

        $file_url = constant("UPLOAD_DIRECTORY").$file_name.rand(1111,9999).".".$file_ext;
        $_SESSION['image_url'] = $file_url;
        move_uploaded_file($file_tmp, $file_url);

        unlink($profile_row['image_url']);
        array_push($update_sequence, " image_url = '$file_url' ");
    }
    if($profile_row['username'] != $_POST['username'])
    {
        $login = $_POST['username'];
        $_SESSION['login_user'] = $login;
        array_push($update_sequence, " username = '$login' ");
    }
    if($profile_row['password'] != $_POST['password'])
    {
        $password = $_POST['password'];
        array_push($update_sequence, " password = '$password' ");
    }
    if($profile_row['email'] != $_POST['email'])
    {
        $email = $_POST['email'];
        array_push($update_sequence, " email = '$email' ");
    }
    if($profile_row['description'] != $_POST['description'])
    {
        $description = $_POST['description'];
        array_push($update_sequence, " description = '$description' ");
    }

    for($i = 0; $i<count($update_sequence)-1; $i = $i + 1)
    {
        $update_sql = $update_sql.$update_sequence[$i].", ";
    }

    $update_sql = $update_sql.$update_sequence[count($update_sequence) - 1];

    $update_sql = $update_sql. " WHERE username='$login_before'";

    try
    {
        mysqli_query($db, $update_sql);
        header('Location: index.php');
    }
    catch(Exception $e)
    {
        echo("Bad SQL");
    }
}

?>

<!doctype html>
<html lang="en">
<head>
    <?php require('views/HeadView.html'); ?>
</head>
<body>
<?php require('views/NavigationView.php') ?>
    <h2>Profile</h2>

    <div class="container">
        <form action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>" method="post" enctype="multipart/form-data">

            <fieldset class="form-group">
                <label for="username">Username:</label>
                <input type="text" class="form-control" name="username" id="username" placeholder="Username" autocomplete="off" value="<?php echo $profile_row['username']; ?>">
            </fieldset>

            <div class="form-group">
                <label for="password">Password:</label>
                <input type="text" class="form-control" name="password" id="password" placeholder="Password" autocomplete="off" value="<?php echo $profile_row['password']; ?>">
            </div>

            <div class="form-group">
                <label for="confirm_password">Confirm Password:</label>
                <input type="text" class="form-control" name="confirm_password" id="confirm_password" placeholder="Confirm Password" autocomplete="off">
            </div>

            <div class="form-group">
                <label for="email">Email:</label>
                <input type="email" class="form-control" name="email" id="email" placeholder="Email" autocomplete="off" value="<?php echo $profile_row['email']; ?>">
            </div>

            <div class="form-group">
                <label for="description">Short about yourself:</label>
                <textarea name="description" class="form-control" id="description" cols="30" rows="10" content="<?php echo $profile_row['description']; ?>"></textarea>
            </div>

            <div class="form-group">
                <label for="avatar">Avatar:</label>
                <input type="file" class="form-control filestyle" name="avatar" id="avatar" data-buttonText="Find file">
            </div>

            <img src="<?php echo $_SESSION['image_url']; ?>" class="img-responsive img-rounded edit-image" alt="Image" width="250" height="250" id="edit-image">

            <button type="submit" class="btn btn-primary btn-block">Update profile</button>

        </form>
    </div>

<?php require('views/JsView.html') ?>
</body>
</html>

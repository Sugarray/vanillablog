<?php
ob_start();
session_start();

include_once('services/SessionChecker.php');
include_once('services/Config.php');
include_once('services/AvatarLoader.php');

if( $_SERVER['REQUEST_METHOD'] == 'GET' )
{
    $post_id = $_GET['post'];

    $articles_sql = "SELECT * FROM Article where Article.id = $post_id ";

    $articles_result = mysqli_query($db, $articles_sql);

    $article = mysqli_fetch_array($articles_result, MYSQLI_ASSOC);

}



?>

<!doctype html>
<html lang="en">
<head>
    <?php include_once('views/HeadView.html'); ?>
</head>
<body>
    <?php require("views/NavigationView.php"); ?>

    <div class="container">
        <h1><?php echo($article['title']); ?></h1>
        <?php echo($article['content']); ?>
    </div>

    <?php require("views/JsView.html") ?>
</body>
</html>

<?php
ob_start();
session_start();

include_once('services/SessionChecker.php');
include_once('services/Config.php');
include_once('services/AvatarLoader.php');

if( $_SERVER['REQUEST_METHOD'] == 'GET' )
{
    $username = $_SESSION['login_user'];
    $user_sql = "SELECT id FROM user WHERE username = '$username' ";
    $result = mysqli_query($db, $user_sql);
    $user_id = mysqli_fetch_array($result,MYSQLI_ASSOC)['id'];

    $articles_sql = "SELECT * FROM Article where Article.user_id = $user_id ";

    $articles_result = mysqli_query($db, $articles_sql);

    $articles_array = [];

    while($row = mysqli_fetch_array($articles_result, MYSQLI_ASSOC))
    {
        $articles_array[] = array("id" => $row['id'], "title"=>$row['title'], "preface"=>$row['preface']);
    }
}


?>

<!doctype html>
<html lang="en">
<?php include_once('views/HeadView.html'); ?>
<body>

<?php require("views/NavigationView.php"); ?>

<div class="container">
    <h2>Articles</h2>
    <?php if(count($articles_array) == 0): ?>
        <div class="alert alert-info" role="alert">
            <h3>No articles yet...want to create <span class="label label-warning"><a href="CreateArticle.php">New one?</a></span></h3>
        </div>
    <?php endif; ?>

    <?php foreach($articles_array as $article): ?>
        <article class="jumbotron">
            <h3><?php echo($article['title']) ?> </h3>
            <p><?php echo($article['preface']) ?></p>
            <a class="btn btn-default pull-right" href="Article.php?post=<?php echo($article['id']) ?>">Read more...</a>
        </article>
    <?php endforeach;?>
</div>

<?php require("views/JsView.html") ?>
</body>
</html>

<?php
session_start();
include_once('services/SessionChecker.php');
include_once('services/Config.php');

if( $_SERVER['REQUEST_METHOD'] == 'POST' )
{

    $username = $_SESSION['login_user'];

    $user_sql = "SELECT id FROM user WHERE username = '$username'";
    $result = mysqli_query($db, $user_sql);
    $row = mysqli_fetch_array($result,MYSQLI_ASSOC);
    $user_id = $row['id'];

    $title = $_POST['title'];
    $preface = $_POST['preface'];
    $content = $_POST['content'];

    $article_sql = "INSERT INTO VanillaBlog.Article (user_id, title, preface, content)
                      VALUES ($user_id, '$title', '$preface', '$content')";

    mysqli_query($db, $article_sql);

    header("location: index.php");
}

?>

<!doctype html>
<html lang="en">
<head>
    <?php require("views/HeadView.html") ?>
</head>
<body>
    <?php require("views/NavigationView.php") ?>

    <div class="container">
        <form action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>" method="post" role="form">
        	<legend>New Article</legend>

        	<div class="form-group">
        		<label for="title">Title of article</label>
        		<input type="text" class="form-control" name="title" id="title" required>
        	</div>

            <div class="form-group">
                <label for="preface">Preface of article</label>
                <input type="text" class="form-control" name="preface" id="preface">
            </div>

            <div class="form-group">
                <label for="content">Article content:</label>
                <textarea  class="form-control" name="content" id="description" cols="30" rows="10"></textarea>
            </div>


        	<button type="submit" class="btn btn-primary btn-block">Create Article</button>
        </form>
    </div>

    <?php require("views/JsView.html") ?>
</body>
</html>

<?php
ob_start();
session_start();
include_once("services/Config.php");



    if($_SERVER['REQUEST_METHOD'] == 'POST')
    {
        $error_username = '';
        $error_password = '';

        if(isset($_POST['register']) && !empty($_POST['username']) && !empty($_POST['password']) &&
            !empty($_POST["password_confirmation"]))
        {
            if($_POST['password'] != $_POST['password_confirmation'])
            {
                $error_password = "Passwords doesn't match";
            }
            else if(strlen($_POST['password']) < 6)
            {
                $error_password = "Password bust be 6 length short minimum";
            }
            else
            {
                $username = mysqli_real_escape_string($db,$_POST['username']);
                $password = mysqli_real_escape_string($db,$_POST['password']);

                $sql = "INSERT INTO user (username, password) VALUES ('$username', $password)";
                mysqli_query($db, $sql);

                header('Location: finishreg.php');
            }
        }
        if(empty($_POST['username']))
        {
            $error_username = "Input username";
        }
        if(empty($_POST['password']))
        {
            $error_password = "Input password";
        }
        else if(empty($_POST['password_confirmation']))
        {
            $error_password = "Repeat password";
        }
    }

?>

<!doctype html>
<html lang="en">
<?php require('views/HeadView.html'); ?>
<body>

<?php require('views/NavigationView.php') ?>

    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Register</div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="Register.php">

                            <div class="form-group">
                                <label class="col-md-4 control-label">Username</label>

                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="username">
                                    <?php if(!empty($error_username)): ?>
                                         <span class="help-block">
                                            <strong><?php echo $error_username; ?></strong>
                                        </span>
                                    <?php endif; ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label">Password</label>

                                <div class="col-md-6">
                                    <input type="password" class="form-control" name="password">
                                    <?php if(!empty($error_password)): ?>
                                        <span class="help-block">
                                            <strong><?php echo $error_password; ?></strong>
                                        </span>
                                    <?php endif; ?>
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Confirm Password</label>

                                <div class="col-md-6">
                                    <input type="password" class="form-control" name="password_confirmation">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary" name="register">
                                        <i class="fa fa-btn fa-user"></i>Register
                                    </button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php require("views/JsView.html") ?>

</body>
</html>

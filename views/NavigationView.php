<nav class="navbar navbar-default navbar-static-top">
    <div class="container">
        <div class="navbar-header">

            <!-- Collapsed Hamburger -->
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <!-- Branding Image -->
            <a class="pull-left brand-font" href="../index.php">
                <img src="../img/Logo.png" width="50" height="50"/>Vanilla Blog</a>
        </div>

        <div class="collapse navbar-collapse" id="app-navbar-collapse">

            <ul class="nav navbar-nav navbar-right">
                <?php if(!isset($_SESSION['login_user'])):?>

                <li>
                    <a href="../Login.php">Login</a>
                </li>
                <li>
                    <a href="../Register.php">Register</a>
                </li>

                <?php else : ?>
                <?php if(isset($_SESSION['image_url'])): ?>
                <li class="image-li">
                    <div class="avatar_small">
                        <img src="<?php echo $_SESSION['image_url']; ?>" class=" img-rounded" alt="Image" width="24" height="24" id="edit-image">
                    </div>
                </li>
                <?php endif; ?>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                        Hello, <?php echo $_SESSION['login_user']?> <span class="caret"></span>
                    </a>

                    <ul class="dropdown-menu" role="menu">
                        <li><a href="../Profile.php"><i class="fa fa-btn fa-user"></i>Profile</a></li>
                        <li><a href="../CreateArticle.php"><i class="fa fa-btn fa-file-text-o"></i>New Article</a></li>

                        <?php if(isset($_SESSION['image_url'])): ?>
                            <li><a href="../AdminPanel.php"><i class="fa fa-btn fa-key"></i>Admin panel</a></li>
                        <?php endif; ?>

                        <li><a href="../services/Logout.php"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>

                    </ul>
                </li>

                <?php endif; ?>
            </ul>
        </div>
    </div>
</nav>